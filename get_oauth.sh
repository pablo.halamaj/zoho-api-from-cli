#! /usr/bin/env bash

### SCOPE: ZohoProjects.tasks.READ,ZohoProjects.tasks.CREATE,ZohoProjects.projects.READ,ZohoProjects.portals.READ,ZohoProjects.timesheets.CREATE,ZohoProjects.timesheets.UPDATE,ZohoProjects.timesheets.READ
if [ -n "${1}" ]; then
	OAUTH_CODE="$1"
fi

if [ -z "${OAUTH_CODE}" ]; then
	echo -e "\n${0} requiere que se le pase un OAUTH_CODE del tipo self-client"
	echo -e "Ir a https://accounts.zoho.com/developerconsole y obtener uno"
	echo -e "More data: https://www.zoho.com/accounts/protocol/oauth-setup.html"
	echo -e "\n\t Ejemplos: ${0} 1000.CTG1XUREOHGZ3xxxxxxxxxxxxENZC227I0H\n"
	echo -e "\n\t Ejemplos: export OAUTH_CODE=1000.CTG1XUREOHGZxxxxxxxxxxxxxxxx27I0H; ${0} \n"
	exit 0
fi

## Obtenemos los TOKENS
OAUT_URL="https://accounts.zoho.com/oauth/v2/token"
REDIRECT_URI="https://www.seti.com"
OAUTH_CLIENT_ID="1000.CTG1XUxxxxxxxxxxxx02O0I5ENZC227I0H"
OAUTH_CLIENT_SECRET="2890ec9b0xxbxxxxxxxxx0a5d1727912439844"
GRANT_TYPE="authorization_code"
#TEMP_JSON="$(/usr/bin/mktemp)"
TEMP_JSON="./oauth.json"

curl -sX POST "${OAUT_URL}?code=${OAUTH_CODE}&redirect_uri=${REDIRECT_URI}&client_id=${OAUTH_CLIENT_ID}&client_secret=${OAUTH_CLIENT_SECRET}&grant_type=${GRANT_TYPE}" > ${TEMP_JSON}

ACCESS_TOKEN="$(cat  ${TEMP_JSON} |jq '.access_token')"
REFRESH_TOKEN="$(cat  ${TEMP_JSON} |jq '.refresh_token')"

echo "ACCES_TOKEN: ${ACCESS_TOKEN}"
echo "REFRESH_TOKEN: ${REFRESH_TOKEN}"

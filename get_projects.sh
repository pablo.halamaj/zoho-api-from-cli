#! /usr/bin/env bash

if [ -n "${1}" ]; then
        PORTAL_ID="$1"
fi

if [ -z "${PORTAL_ID}" ]; then
        echo -e "\n${0} requiere que se le pase un PORTAL_ID"
        echo -e "Usar get_portals.sh para obtener uno"
        echo -e "\n\t Ejemplo: ${0} 692920622\n"
        echo -e "\n\t Ejemplos: export PORTAL_ID=692920622; ${0} \n"
        exit 0
fi


## URL de los Portales
PRJ_API="https://projectsapi.zoho.com/restapi"

echo -e "\nA continuacion se enumerarán todos los Proyectos disponibles\n"
curl -sH "Authorization: Bearer ${ACCESS_TOKEN}" ${PRJ_API}/portal/${PORTAL_ID}/projects/ | jq '.projects[] | {id: .id_string, name: .name}'

#! /usr/bin/env bash

### SCOPE: ZohoProjects.tasks.READ,ZohoProjects.tasks.CREATE,ZohoProjects.projects.READ,ZohoProjects.portals.READ,ZohoProjects.timesheets.CREATE,ZohoProjects.timesheets.UPDATE,ZohoProjects.timesheets.READ

if [ -n "${1}" ]; then
	REFRESH_TOKEN="$1"
else
	source zoho.vars
fi

if [ -z "${REFRESH_TOKEN}" ]; then
	echo -e "\n${0} requiere que se le pase un REFRESH_TOKEN"
	echo -e "\n\t Ejemplos: ${0} 1000.803b7d37f00f7879e24957d50a2c8d9xxxxxxxxxxxxxxxxxx7ec1cde7\n"
	echo -e "\n\t Ejemplos: export REFRESH_TOKEN=1000.803b7d37f00f7879e24xxxxxxxxxxxxxxx7c98a5bd7cfc7ec1cde7; ${0} \n"
	exit 1 
fi

## Obtenemos los TOKENS
OAUT_URL="https://accounts.zoho.com/oauth/v2/token"
REDIRECT_URI="https://www.seti.com"
OAUTH_CLIENT_ID="1000.CTG1XUREOHGxxxxxxxxxxxxxxxxxxC227I0H"
OAUTH_CLIENT_SECRET="2890ec9b0da313febxxxxxxxxxxxxxxxx727912439844"

curl -sX POST "${OAUT_URL}?refresh_token=${REFRESH_TOKEN}&client_id=${OAUTH_CLIENT_ID}&client_secret=${OAUTH_CLIENT_SECRET}&grant_type=refresh_token" |jq

echo -e "\nAjuste el archivo zoho.vars con el nuevo access_token"

El orden de Uso es:

* Crearte un Client ID para api
	* ver https://www.zoho.com/accounts/protocol/oauth-setup.html
* Luego conseguir un Tocker del tipo Self-client
	* Usar de Scope "ZohoProjects.tasks.READ,ZohoProjects.tasks.CREATE,ZohoProjects.projects.READ,ZohoProjects.portals.READ,ZohoProjects.timesheets.CREATE,ZohoProjects.timesheets.UPDATE,ZohoProjects.timesheets.READ"
* Ejecutar get_oauth.sh para obtener los tokens
* Ejecutar get_portals.sh para obtener los portals.
* Ejecutar get_projects.sh para obtener los proyectos
* Ejecutar get_tasks.sh para obtener los tasks
* Cargar las variables de zoho.vars
* ejecutar put_tasks.sh para cargar las horas en las tareas, una por vez
* Si el Token se vence (dura 1 hora) usar refresh.token.sh para renovarlo.

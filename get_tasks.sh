#! /usr/bin/env bash

if [ -n "${1}" ]; then
        PRJ_ID="$1"
fi

if [ -z "${PRJ_ID}" ]; then
        echo -e "\n${0} requiere que se le pase un PRJ_ID"
        echo -e "Usar get_project.sh para obtener uno"
        echo -e "\n\t Ejemplo: ${0} 14691090xxxxxxx040013\n"
        echo -e "\n\t Ejemplos: export PRJ_ID=1469109000xxxxxx0013; ${0} \n"
        exit 0
fi


## URL de los Portales
PRJ_API="https://projectsapi.zoho.com/restapi"

echo -e "\nA continuacion se enumerarán todos los Proyectos disponibles\n"
curl -sH "Authorization: Bearer ${ACCESS_TOKEN}" ${PRJ_API}/portal/${PORTAL_ID}/projects/${PRJ_ID}/tasks/ | jq '.tasks[] | {id: .id_string, name: .name, horas: .log_hours.billable_hours}' 

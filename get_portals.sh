#! /usr/bin/env bash

if [ -n "${1}" ]; then
        ACCESS_TOKEN="$1"
fi

if [ -z "${ACCESS_TOKEN}" ]; then
        echo -e "\n${0} requiere que se le pase un ACCESS_TOKEN"
        echo -e "Usar get_oauth.sh para obtener uno"
        echo -e "\n\t Ejemplo: ${0} 1000.CTG1XUREOHGZxxxxxxxxxxxxxxxx27I0H\n"
        echo -e "\n\t Ejemplos: export ACCESS_TOKEN=1000.CTG1XUREOHGZxxxxxxxxxxxxx227I0H; ${0} \n"
        exit 0
fi

## URL de los Portales
PRJ_API="https://projectsapi.zoho.com/restapi"

echo -e "\nA continuacion se enumerarán todos los Portales disponibles\n"
curl -sH "Authorization: Bearer ${ACCESS_TOKEN}" ${PRJ_API}/portals/ |jq '.portals[] | {id: .id_string, name: .name }'


#! /usr/bin/env bash

if [ -n "${1}" ] && [ -n "${2}" ]; then
        HORAS="$2"
	TASKS_ID="$1"
fi

if [ -z "${HORAS}" ] || [ -z "${TASKS_ID}"]; then
        echo -e "\n${0} requiere que se le pase una cantidad un TASKS_ID y cantidad de HORAS"
        echo -e "Usar get_project.sh para obtener uno"
        echo -e "\n\t Ejemplo: ${0} 1469109000xxxxxxxxx0013 8:34\n"
        exit 1
fi

### Incluimos todas las variables desde un archivo
source zoho.vars

## URL de los Portales
PRJ_API="https://projectsapi.zoho.com/restapi"

echo -e "\nA continuacion se incluiran ${HORAS} Billable en la tarea ${TASKS_ID}\n"
### Agregamos Horas a una Task
curl -sX POST -H "Authorization: Bearer ${ACCESS_TOKEN}" "${PRJ_API}/portal/${PORTAL_ID}/projects/${PRJ_ID}/tasks/${TASKS_ID}/logs/?&date=${DATE}&owner=${ZOHO_ID}&bill_status=${TIPO_DE_HORA}&hours=${HORAS}" |jq
